//
//  Event.swift
//  entitymapping
//
//  Created by Sascha Mundstein on 20/08/14.
//  Copyright (c) 2014 Sascha Mundstein. All rights reserved.
//

import Foundation
import CoreData

@objc(Event)
class Event: NSManagedObject {

    @NSManaged var code: NSNumber
    @NSManaged var needsUpdating: NSNumber
    @NSManaged var price: NSNumber
    @NSManaged var timeStamp: NSDate

}
