//
//  Policy.swift
//  entitymapping
//
//  Created by Sascha Mundstein on 20/08/14.
//  Copyright (c) 2014 Sascha Mundstein. All rights reserved.
//

import UIKit
import CoreData

class Policy: NSEntityMigrationPolicy {
    
    override func createDestinationInstancesForSourceInstance(sInstance: NSManagedObject!, entityMapping mapping: NSEntityMapping!, manager: NSMigrationManager!, error: NSErrorPointer) -> Bool {
        
        
        // This is how it should work.
        // But I am getting this breakpoint: http://stackoverflow.com/q/24841856/427083
        // The fixes are not working.
        
/*
var dInstance : Event! = NSEntityDescription.insertNewObjectForEntityForName(mapping.destinationEntityName, inManagedObjectContext: manager.destinationContext) as Event

dInstance.code = sInstance.valueForKey("code") as NSNumber
dInstance.price = sInstance.valueForKey("price") as NSNumber
dInstance.timeStamp = sInstance.valueForKey("timeStamp") as NSDate
dInstance.needsUpdating = true

manager.associateSourceInstance(sInstance, withDestinationInstance: dInstance, forEntityMapping: mapping)
*/
        
        var dInstance = NSEntityDescription.insertNewObjectForEntityForName(mapping.destinationEntityName, inManagedObjectContext: manager.destinationContext) as NSManagedObject
        
        dInstance.setValue(sInstance.valueForKey("code"), forKey: "code")
        dInstance.setValue(sInstance.valueForKey("price"), forKey: "price")
        dInstance.setValue(sInstance.valueForKey("timeStamp"), forKey: "timeStamp")
        dInstance.setValue(true, forKey: "needsUpdating")
        
        manager.associateSourceInstance(sInstance, withDestinationInstance: dInstance, forEntityMapping: mapping)


        return true
    }

}
